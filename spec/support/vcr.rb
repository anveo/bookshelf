require "vcr"

VCR.configure do |config|
  config.cassette_library_dir = "spec/fixtures/vcr_cassettes"
  config.hook_into :webmock
  #config.ignore_localhost = true
  config.ignore_request do |request|
    request.uri =~ /__identify__$/
  end
end


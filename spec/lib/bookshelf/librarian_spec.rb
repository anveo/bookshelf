require "rails_helper"

describe Bookshelf::Librarian do
  subject { described_class }
  describe "#search" do
    it "returns correct number of default results" do
      VCR.use_cassette("googlebooks/hp-full-results") do
        books = subject.search("harry potter")
        expect(books.count).to eq(subject::DEFAULT_PARAMS[:maxResults])
      end
    end

    it "handles an empty result list" do
      VCR.use_cassette("googlebooks/empty-results") do
        books = subject.search("asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdffasdf")
        expect(books.count).to eq(0)
      end
    end
  end
end


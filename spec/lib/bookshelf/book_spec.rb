require "rails_helper"

describe Bookshelf::Book do

  describe ".enhance_title_from_volume_info" do
    subject { described_class.new }

    it "assigns a string" do
      subject.enhance_title_from_volume_info({"title" => "Harry Potter"})
      expect(subject.title).to eq("Harry Potter")
    end

    it "concatinates an array" do
      subject.enhance_title_from_volume_info({"title" => ["Harry Potter", "Chamber of Secrets"]})
      expect(subject.title).to eq("Harry Potter: Chamber of Secrets")
    end

    it "concatinates a subtitle" do
      subject.enhance_title_from_volume_info({"title" => "Harry Potter", "subtitle" => "A Wizard's Guide"})
      expect(subject.title).to eq("Harry Potter: A Wizard's Guide")
    end
  end

  describe ".enhance_images_from_volume_info" do
    subject { described_class.new }

    it "assign's an image when present" do
      subject.enhance_images_from_volume_info({"imageLinks" => {"thumbnail" => "http://example.com/image.png"}})
      expect(subject.image_url).to eq("http://example.com/image.png")
    end

    it "doesn't fail on missing key" do
      subject.enhance_images_from_volume_info({})
      expect(subject.image_url).to be_nil
    end
  end
end


require "rails_helper"

describe BooksHelper do
  describe "#star_rating" do
    it "returns blank if book has no rating" do
      book = Bookshelf::Book.new
      book.rating = nil

      expect(helper.star_rating(book)).to eq("")
    end

    it "return a full number of stars" do
      book = Bookshelf::Book.new
      book.rating = 5.0

      expect(helper.star_rating(book).to_str.scan(/fa-star/).count).to eq(5)
    end

    it "returns half stars" do
      book = Bookshelf::Book.new
      book.rating = 4.5

      expect(helper.star_rating(book).to_str.scan(/fa-star-half/).count).to eq(1)
    end
  end
end


class BookshelfController < ApplicationController
  def index
    @books = []

    if params[:q].present?
      begin
        @books = Bookshelf::Librarian.search(params[:q])
      rescue Bookshelf::LibrarianError => e
        flash.now[:error] = e.message
      end
    end
  end
end


module ApplicationHelper
  def flash_css_class(flash_key_name)
    case flash_key_name
    when "error"
      " alert-danger"
    else
      ""
    end
  end
end

module BooksHelper
  def star_rating(book)
    return "" if book.rating.blank?

    content_tag(:div, class: "book-rating") do
      (1..5).each do |i|
        if book.rating >= i
          concat content_tag(:i, nil, class: "fa fa-star fa-2x")
          concat content_tag(:i, nil, class: "fa fa-star-half-o fa-2x") if (book.rating - i == 0.5)
        end
      end
    end
  end
end

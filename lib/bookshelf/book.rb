module Bookshelf
  class Book
    attr_accessor :id, :title, :description, :authors, :image_url, :rating

    def from_json(json)
      volume_info = json["volumeInfo"]

      @id          = json["id"]
      @description = volume_info["description"]
      @authors     = volume_info["authors"].join(", ") if volume_info["authors"]
      @rating      = volume_info["averageRating"].to_f if volume_info["averageRating"]

      enhance_title_from_volume_info(volume_info)
      enhance_images_from_volume_info(volume_info)
    end

    def enhance_title_from_volume_info(volume_info)
      @title       = [volume_info["title"]].flatten.join(": ")
      if volume_info.key?("subtitle")
        @title += ": #{volume_info["subtitle"]}"
      end
    end

    def enhance_images_from_volume_info(volume_info)
      if volume_info.key?("imageLinks")
        @image_url   = volume_info["imageLinks"]["thumbnail"]
      end
    end

    def self.from_json(json)
      book = new
      book.from_json(json)
      book
    end
  end
end


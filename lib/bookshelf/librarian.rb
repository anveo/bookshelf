require "json"

module Bookshelf
  class LibrarianError < StandardError; end

  class Librarian
    include HTTParty
    base_uri "https://www.googleapis.com/books/v1"
    format :json

    DEFAULT_PARAMS = {
      page: 1,
      maxResults: 20
    }

    def self.search(q)
      params = {
        q: q,
      }.reverse_merge(DEFAULT_PARAMS)

      # TODO additional params

      begin
        resp = get("/volumes?#{params.to_query}")
      rescue HTTParty::Error
        raise LibrarianError, "Error retrieving book list."
      end

      json = JSON.parse(resp.body)
      json_to_book_list(json)
    end

    #def self.get_volume(id)
      #begin
        #get("/volumes/#{id}")
      #rescue HTTParty::Error
        #raise LibrarianError, "Error retrieving book."
      #end
    #end

    private_class_method def self.json_to_book_list(json)
      return [] unless json.key?("items")
      books = []

      json["items"].each do |item|
        books << Book.from_json(item)
      end

      books
    end
  end
end

